#**BRIOHR-Kubernetes/Prometheus/Grafana Dashboard Access**
-------------------------------------------------------------

As part of security control, we do not publish Kubernetes, Prometheus, Grafana and Alertmanager dashboard publicly accessible. To access these dashboard, we will proxy it over to your local machine.

Please follow below steps to gain access into the dashboards. 

###**Prerequisite**

You need an AWS account with appropriate privilages to configure AWS CLI in your local machine. For further enquiry, you can reach out to [support@briohr.com](support@briohr.com)

###**Installing AWS CLI and Kubelet**
========================

####AWS CLI
Please follow below guide to have AWS CLI installed on your local machine. 

- Installation Guide for [Linux](https://docs.aws.amazon.com/cli/latest/userguide/install-linux.html)
- Installation Guide for [Windows](https://docs.aws.amazon.com/cli/latest/userguide/install-windows.html)

Once installed you should be able to run below command on the console:-

``` aws version``` or ```aws --version```

####Kubelet
Please follow below guide to have Kubelet installed on your local machine.

- Installation Guide for [Kubelet](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html)

Once installed you should be able to run elow command on the console:-

``` kubectl version```

###**Configuring AWS and Kubelet**
========================

First, you need to configure your AWS-CLI to have communication over our clusters. To do that you will need to run below command:-

``` aws configure```

and provide below information,

- **AWS access Key ID** - this should be provided by [support@briohr.com](support@briohr.com) or the Administrator
- **AWS Secret Access Key** - this should be provided by [support@briohr.com](support@briohr.com) or the Administrator
- **Default Region Name** - <cluster_region>
- **Default output format** - text

Or you can also follow this guide provided by AWS - [Configuring CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-configure.html)

Next, you need to update your Kubelet configuration for the clusters.

``` aws eks --region <cluster_region> update-kubeconfig --name <cluster_name>```

Once configured, you type ```kubelet get svc``` to list out all the services listed under our Kubernetes Cluster. 

###**Accessing the Dashboard**
========================

####Prometheus
Enter this command at the console
``` kubectl port-forward -n monitoring svc/prom-op-prometheus-operato-prometheus 9090```

Then go the to your browser and request for http://localhost:9090/

Request Prometheus credentials from [support@briohr.com](support@briohr.com)

####Grafana
Enter this command at the console
``` kubectl port-forward -n monitoring deploy/prom-op-grafana 3000```

Then go the to your browser and request for http://localhost:3000/

Request Grafana credentials from [support@briohr.com](support@briohr.com)

####AlertManager
Enter this command at the console
``` kubectl port-forward -n monitoring svc/prom-op-prometheus-operato-alertmanager 9093```

Then go the to your browser and request for http://localhost:9093/

Request AlertManager credentials from [support@briohr.com](support@briohr.com)

####Kubernetes Dashboard
Enter this command at the console
``` kubectl proxy```

Then go the to your browser and request for http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/#!/login

Request Kubernetes Token from [support@briohr.com](support@briohr.com)

###**Troubleshooting & FAQ**
========================

If you have any issues with installing/configuring any of the guide above, please reach out to our administrator. 